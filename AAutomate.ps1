function Start-AutomateCheck {
    <#
    .SYNOPSIS
        This script will ensure that the ConnectWise Automate agent stays connected.

    .DESCRIPTION
        This script will check to make sure the computer is online and that the agent has checked-in
        within the last $HoursOffline. If not it will attempt to restart the services. If that fails,
        it will backup the install and reinstall the agent.

    .PARAMETER HoursOffline
        The amount of time allowed from last check-in before corrective action.
        Default = -2

    .PARAMETER TaskName
        The name of the scheduled script that is created.
        Default = 'AAutomate'

    .PARAMETER InstallerToken
        Token used to download the installer.

    .OUTPUTS
        Will output a backup of the registry to 'HKLM\SOFTWARE\LabTech\ServiceBackup'
        also a copy of the install directory at "$((Get-LTServiceInfo).BasePath)Backup"

    .NOTES
        Version:        1.1
        Author:         Chris Taylor
        Creation Date:  8/3/2020
        Purpose/Change: Support for InstallerToken

        Version:        1.0
        Author:         Chris Taylor
        Creation Date:  5/11/2017
        Purpose/Change: Initial script development

    .LINK
      labtechconsulting.com
  
    .EXAMPLE
    <Example goes here. Repeat this attribute for more than one example>
    #>
    [CmdletBinding()]
    Param (
        [Parameter(ParameterSetName = "Install",Mandatory=$true)]
        [string]$Server,
        [Parameter(ParameterSetName = "Install",Mandatory=$true)]
        [int]$LocationID,
        [Parameter(ParameterSetName = "Install",Mandatory=$true)]
        [Parameter(ParameterSetName = "Checkup",Mandatory=$true)]
        [string]$InstallerToken,
        [Parameter(ParameterSetName = "Install")]
        [int]$HoursOffline = -2,
        [Parameter(ParameterSetName = "Install")]
        [string]$TaskName = 'AAutomate'
    )

    #Script Version
    $ScriptVersion = "1.0"
 
    #region-[Initializations]--------------------------------------------------------
        #requires -version 3
        #Set Error Action to Silently Continue
        $ErrorActionPreference = "Stop"

        #Dot Source required Function Libraries
        try {
            (new-object Net.WebClient).DownloadString('https://raw.githubusercontent.com/ChrisTaylorRocks/Powershell-Logging/master/Powershell-Logging.ps1') | Invoke-Expression
            (new-object Net.WebClient).DownloadString('https://bit.ly/LTPoSh') | Invoke-Expression
        }
        catch {
            Write-Output 'ERROR: There was an issue durning initialization.'
            Write-Output $_
            EXIT 1
        }        

    #endregion Initializations
 
    #region-[Declarations]----------------------------------------------------------
    
        #Log File Info
        $LogPath =  "$env:windir\LTSvcBackup\$($TaskName).log"
        $Status = 'Success'

    #endregion Declarations
 
    #region-[Execution]------------------------------------------------------------
 
        Set-LogSettings -LogPath $LogPath -Status $Status 
        Start-Log -ScriptVersion $ScriptVersion -Append -ToScreen

        Write-Warning "***************************************************************************************************"
        Write-Warning "* This script should be ran from the scheduled task, $TaskName."
        Write-Warning "* Only run from console for testing purposes."
        Write-Warning "***************************************************************************************************"

        #Is LT installed?
        if(Get-Service LTService -ErrorAction SilentlyContinue){
            try {
                # If server is supplied make sure they match
                $CurrentServer = (Get-LTServiceInfo).'Server Address'.split('|')
                if($Server -and $LocationID -and $CurrentServer -notcontains $Server){                
                    Write-LogError -TimeStamp -ToScreen -Message "Wrong install server $CurrentServer, reinstalling."
                    Redo-LTService -Server $Server -LocationID $LocationID -InstallerToken $InstallerToken
                }

                # Get last contact date
                try { [datetime]$LastContact = (Get-LTServiceInfo).LastSuccessStatus }
                catch { [datetime]$LastContact = (Get-Date).AddYears(-1) }
                
                Write-LogInfo -TimeStamp -ToScreen -Message "Last checkin time : $Lastcontact"

                # If last contact was more than $HoursOffline take corrective action
                if($LastContact -lt (Get-Date).AddHours(($HoursOffline))) {
                    Write-LogInfo -TimeStamp -ToScreen -Message "Agent has NOT checked in within the last $HoursOffline hour(s)."
                    #Is the server online?
                    if(!(Test-LTPorts -Quiet)){
                        Write-LogError -TimeStamp -ToScreen -Message "Server is not avaliable." -ExitGracefully
                    }

                    Write-LogInfo -TimeStamp -ToScreen -Message "Restarting services."
                    Restart-LTService

                    Write-LogInfo -TimeStamp -ToScreen -Message "Waiting for agent checkin."
                    $startDate = Get-Date
                    while ($LastContact -lt (Get-Date).AddHours($HoursOffline) -and $startDate.AddMinutes(2) -gt (Get-Date)){
                        Start-Sleep -Seconds 2
                        [datetime]$LastContact = (Get-LTServiceInfo).LastSuccessStatus
                    }

                    if($LastContact -lt (Get-Date).AddHours($HoursOffline)){
                        Write-LogInfo -TimeStamp -ToScreen -Message "Still not connecting after restart."
                        Write-LogInfo -TimeStamp -ToScreen -Message "Backing up, reinstalling, and hiding agent."
                        Redo-LTService -Backup -Hide
                        Write-LogInfo -TimeStamp -ToScreen -Message "Completed reinstall."
                    }
                }
            }
            catch {
                $Message = @()
                $Message += 'There was an issue during service repair.'
                $Message += $_
                Write-LogError -TimeStamp -ToScreen -Message ($Message | Out-String) -ExitGracefully
            }            
        }
        else {
            try {
                Write-LogInfo -TimeStamp -ToScreen -Message "Agent install not found, installing."
                # If we provide installer info use that
                if ($Server -and $LocationID) {
                    Write-LogInfo -TimeStamp -ToScreen -Message "Attempting agent install."
                    Redo-LTService -Server $Server -LocationID $LocationID -InstallerToken $InstallerToken
                }
                # Look for current settings
                else {
                    $Settings = Get-LTServiceInfo
                    if(!$Settings){
                        $Backup = $false
                        $Settings = Get-LTServiceInfoBackup
                    }
                    if($Settings){
                        Write-LogInfo -TimeStamp -ToScreen -Message "Backing up install."
                        if($Backup -ne $false){
                            New-LTServiceBackup -ErrorAction SilentlyContinue
                        }            
                        Write-LogInfo -TimeStamp -ToScreen -Message "Reinstalling agent."
                        Redo-LTService -Server ($Settings.'Server Address').split('|')[0] -LocationID $Settings.LocationID -Hide
                    }
                    else{
                        Write-LogError -TimeStamp -ToScreen -Message "Unable to find install settings."
                        $Status = 'Failed'
                    }
                }            
            }
            catch {
                $Message = @()
                $Message += 'There was an issue during install.'
                $Message += $_
                Write-LogError -TimeStamp -ToScreen -Message ($Message | Out-String) -ExitGracefully
            }
        }

        #Setup scheduled task
        try {
            try {
                [xml]$CurrentTask = schtasks /QUERY /XML /TN $TaskName
            } catch{}
            if ($CurrentTask.Task.Actions.Exec.Arguments -notmatch $InstallerToken) {
                Write-LogInfo -ToScreen -TimeStamp -Message "Backing up config." 
                New-LTServiceBackup -ErrorAction SilentlyContinue
                Write-LogInfo -ToScreen -TimeStamp -Message "Creating scheduled task, $TaskName."
                
                try{ $null = schtasks /DELETE /TN "$TaskName" /F 2>&1 }
                catch{}
                        
                [xml]$TaskXML = @"
<?xml version="1.0" encoding="UTF-16"?>
<Task version="1.2" xmlns="http://schemas.microsoft.com/windows/2004/02/mit/task">
    <RegistrationInfo>
    <Description>Check of management service.</Description>
    <URI>\$TaskName</URI>
    </RegistrationInfo>
    <Principals>
    <Principal id="Author">
        <UserId>S-1-5-18</UserId>
        <RunLevel>HighestAvailable</RunLevel>
    </Principal>
    </Principals>
    <Settings>
    <DisallowStartIfOnBatteries>true</DisallowStartIfOnBatteries>
    <StopIfGoingOnBatteries>true</StopIfGoingOnBatteries>
    <MultipleInstancesPolicy>IgnoreNew</MultipleInstancesPolicy>
    <IdleSettings>
        <Duration>PT10M</Duration>
        <WaitTimeout>PT1H</WaitTimeout>
        <StopOnIdleEnd>false</StopOnIdleEnd>
        <RestartOnIdle>false</RestartOnIdle>
    </IdleSettings>
    </Settings>
    <Triggers>
    <TimeTrigger>
        <StartBoundary>$(Get-Date -Format "yyy-MM-ddTHH:mm:ssK")</StartBoundary>
        <Repetition>
        <Interval>PT6H</Interval>
        <Duration>P7300D</Duration>
        <StopAtDurationEnd>true</StopAtDurationEnd>
        </Repetition>
        <RandomDelay>PT6H</RandomDelay>
    </TimeTrigger>
    </Triggers>
    <Actions Context="Author">
    <Exec>
        <Command>C:\WINDOWS\System32\WindowsPowerShell\v1.0\powershell.exe</Command>
        <Arguments>-NoProfile -WindowStyle Hidden -command "&amp;{[Net.ServicePointManager]::SecurityProtocol=[Net.ServicePointManager]::SecurityProtocol -bor [Net.SecurityProtocolType]::Tls12; (new-object Net.WebClient).DownloadString('https://assist.itnow.net/Labtech/Transfer/Scripts/LT/AAutomate.ps1') | iex;Start-AutomateCheck -InstallerToken '$InstallerToken'}"</Arguments>
    </Exec>
    </Actions>
</Task>
"@
                $TaskPath = "$env:temp\task.xml"
                $TaskXML.Save($TaskPath)

                schtasks /CREATE /tn "$TaskName" /xml "$TaskPath" /ru SYSTEM
    
                Write-LogInfo -ToScreen -TimeStamp -Message "AAutomate is now setup."
                Write-LogInfo -ToScreen -TimeStamp -Message "If you ever wish to start or stop this monitor do so from the scheduled task."
            }
        }
        catch {
            $Message = @()
            $Message += 'There was an issue during scheduled task creation.'
            $Message += $_
            Write-LogError -TimeStamp -ToScreen -Message ($Message | Out-String) -ExitGracefully
        }

        Stop-Log -Status $Status -ToScreen

    #endregion Execution
}